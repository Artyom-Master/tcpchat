#include "chatwindow.h"
#include "ui_chatwindow.h"
#include "chatclient.h"
#include "server.h"
#include <QStandardItemModel>
#include <QInputDialog>
#include <QMessageBox>

ChatWindow::ChatWindow(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::ChatWindow)
    , m_chatClient(new ChatClient(this))
    , m_server(new Server(this))
    , m_chatModel(new QStandardItemModel(this)) // create the model to hold the messages
{
    ui->setupUi(this);
    m_chatModel->insertColumn(0);
    ui->chatView->setModel(m_chatModel);
    connect(m_chatClient, &ChatClient::connected, this, &ChatWindow::connectedToServer);
    connect(m_chatClient, &ChatClient::messageReceived, this, &ChatWindow::messageReceived);
    connect(m_chatClient, &ChatClient::disconnected, this, &ChatWindow::disconnectedFromServer);
    connect(m_chatClient, &ChatClient::error, this, &ChatWindow::error);
    connect(m_server, &Server::messageReceived, this, &ChatWindow::messageReceived);
    connect(m_server, &Server::unlock, this, &ChatWindow::unlockServerInterface);
    connect(ui->startStopServerButton, &QPushButton::clicked, this, &ChatWindow::toggleStartServer);
    connect(ui->connectButton, &QPushButton::clicked, this, &ChatWindow::attemptConnection);
    connect(ui->sendButton, &QPushButton::clicked, this, &ChatWindow::sendMessage);
    connect(ui->messageEdit, &QLineEdit::returnPressed, this, &ChatWindow::sendMessage);
}

ChatWindow::~ChatWindow()
{
    delete ui;
}

void ChatWindow::toggleStartServer()
{
    if (m_server->isListening()) {
        m_server->stopServer();
        ui->connectButton->setEnabled(true);
        ui->sendButton->setEnabled(false);
        ui->messageEdit->setEnabled(false);
        ui->chatView->setEnabled(false);
        ui->startStopServerButton->setText(tr("Start Server"));
    } else {
        if (!m_server->listen(QHostAddress::Any, 1967)) {
            QMessageBox::critical(this, tr("Error"), tr("Unable to start the server"));
            return;
        }
        ui->startStopServerButton->setText(tr("Stop Server"));
        ui->connectButton->setEnabled(false);
        ui->chatView->setEnabled(true);
        const int newRow = m_chatModel->rowCount();
        m_chatModel->insertRow(newRow);
        m_chatModel->setData(m_chatModel->index(newRow, 0), "Server launched");
        m_chatModel->setData(m_chatModel->index(newRow, 0), int(Qt::AlignRight | Qt::AlignVCenter), Qt::TextAlignmentRole);
        ui->chatView->scrollToBottom();
    }
}

void ChatWindow::attemptConnection()
{
    if(toConnect)
    {
        const QString hostAddress = QInputDialog::getText(
            this
            , tr("Chose Server")
            , tr("Server Address")
            , QLineEdit::Normal
            , QStringLiteral("127.0.0.1")
        );
        if (hostAddress.isEmpty())
            return;
        toConnect = false;
        ui->connectButton->setText(tr("Disconnect"));
        ui->startStopServerButton->setEnabled(false);
        m_chatClient->connectToServer(QHostAddress(hostAddress), 1967);
    }
    else
    {
        toConnect = true;
        ui->connectButton->setText(tr("Connect"));
        ui->startStopServerButton->setEnabled(true);
        m_chatClient->sendMessage("Client disconnected");
        m_chatClient->disconnectFromHost();
    }
}

void ChatWindow::connectedToServer()
{
    ui->sendButton->setEnabled(true);
    ui->messageEdit->setEnabled(true);
    ui->chatView->setEnabled(true);
    ui->startStopServerButton->setEnabled(false);
}

void ChatWindow::unlockServerInterface()
{
    ui->sendButton->setEnabled(true);
    ui->messageEdit->setEnabled(true);
}

void ChatWindow::messageReceived(const QString &text)
{
    int newRow = m_chatModel->rowCount();
    m_chatModel->insertRow(newRow);
    m_chatModel->setData(m_chatModel->index(newRow, 0), text);
    m_chatModel->setData(m_chatModel->index(newRow, 0), int(Qt::AlignLeft | Qt::AlignVCenter), Qt::TextAlignmentRole);
    ui->chatView->scrollToBottom();
    if(text == "Server disconnected") attemptConnection();
    if(text == "Client disconnected" && m_server->isListening()) toggleStartServer();
}

void ChatWindow::sendMessage()
{
    if(!m_server->isListening()) m_chatClient->sendMessage(ui->messageEdit->text());
    else m_server->sendMessage(ui->messageEdit->text());
    const int newRow = m_chatModel->rowCount();
    m_chatModel->insertRow(newRow);
    m_chatModel->setData(m_chatModel->index(newRow, 0), ui->messageEdit->text());
    m_chatModel->setData(m_chatModel->index(newRow, 0), int(Qt::AlignRight | Qt::AlignVCenter), Qt::TextAlignmentRole);
    ui->messageEdit->clear();
    ui->chatView->scrollToBottom();
}

void ChatWindow::disconnectedFromServer()
{
    ui->sendButton->setEnabled(false);
    ui->messageEdit->setEnabled(false);
    ui->chatView->setEnabled(false);
    ui->connectButton->setEnabled(true);
    ui->startStopServerButton->setEnabled(true);
}

void ChatWindow::error(QAbstractSocket::SocketError socketError)
{
    // show a message to the user that informs of what kind of error occurred
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
    case QAbstractSocket::ProxyConnectionClosedError:
        return; // handled by disconnectedFromServer
    case QAbstractSocket::ConnectionRefusedError:
    {
        QMessageBox::critical(this, tr("Error"), tr("The host refused the connection"));
        toConnect = true;
        ui->connectButton->setText(tr("Connect"));
        ui->startStopServerButton->setEnabled(true);
        m_chatClient->disconnectFromHost();
    }break;
    case QAbstractSocket::ProxyConnectionRefusedError:
        QMessageBox::critical(this, tr("Error"), tr("The proxy refused the connection"));
        break;
    case QAbstractSocket::ProxyNotFoundError:
        QMessageBox::critical(this, tr("Error"), tr("Could not find the proxy"));
        break;
    case QAbstractSocket::HostNotFoundError:
        QMessageBox::critical(this, tr("Error"), tr("Could not find the server"));
        break;
    case QAbstractSocket::SocketAccessError:
        QMessageBox::critical(this, tr("Error"), tr("You don't have permissions to execute this operation"));
        break;
    case QAbstractSocket::SocketResourceError:
        QMessageBox::critical(this, tr("Error"), tr("Too many connections opened"));
        break;
    case QAbstractSocket::SocketTimeoutError:
        QMessageBox::warning(this, tr("Error"), tr("Operation timed out"));
        return;
    case QAbstractSocket::ProxyConnectionTimeoutError:
        QMessageBox::critical(this, tr("Error"), tr("Proxy timed out"));
        break;
    case QAbstractSocket::NetworkError:
        QMessageBox::critical(this, tr("Error"), tr("Unable to reach the network"));
        break;
    case QAbstractSocket::UnknownSocketError:
        QMessageBox::critical(this, tr("Error"), tr("An unknown error occured"));
        break;
    case QAbstractSocket::UnsupportedSocketOperationError:
        QMessageBox::critical(this, tr("Error"), tr("Operation not supported"));
        break;
    case QAbstractSocket::ProxyAuthenticationRequiredError:
        QMessageBox::critical(this, tr("Error"), tr("Your proxy requires authentication"));
        break;
    case QAbstractSocket::ProxyProtocolError:
        QMessageBox::critical(this, tr("Error"), tr("Proxy comunication failed"));
        break;
    case QAbstractSocket::TemporaryError:
    case QAbstractSocket::OperationError:
        QMessageBox::warning(this, tr("Error"), tr("Operation failed, please try again"));
        return;
    default:
        Q_UNREACHABLE();
    }
    // enable the button to connect to the server again
    ui->connectButton->setEnabled(true);
    // disable the ui to send and display messages
    ui->sendButton->setEnabled(false);
    ui->messageEdit->setEnabled(false);
    ui->chatView->setEnabled(false);
}
