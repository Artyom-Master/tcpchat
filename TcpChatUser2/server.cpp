#include "server.h"
#include <functional>

Server::Server(QObject *parent)
    : QTcpServer(parent)
    , m_serverSocket(new QTcpSocket(this))
{
    connect(m_serverSocket, &QTcpSocket::readyRead, this, &Server::onReadyRead);

}

void Server::incomingConnection(qintptr socketDescriptor)
{
    if (!m_serverSocket->setSocketDescriptor(socketDescriptor)) {
        m_serverSocket->deleteLater();
        return;
    }
    QString connected = "Client connected", connectStatus = "You are connected to Server";
    messageReceived(connected);
    sendMessage(connectStatus);
    unlock();
}

void Server::sendMessage(const QString &text)
{
    if (text.isEmpty())
        return;
    QTextStream serverStream(m_serverSocket);
    serverStream << text;
}

void Server::onReadyRead()
{
    QString data;
    QTextStream serverStream(m_serverSocket);
    data = serverStream.readAll();
    emit messageReceived(data);
}

void Server::stopServer()
{
    QString disconnected = "Server disconnected";
    sendMessage(disconnected);
    close();
}


