#ifndef SERVER_H
#define SERVER_H

#include <QTcpServer>
#include <QTcpSocket>
class Server : public QTcpServer
{
    Q_OBJECT
    Q_DISABLE_COPY(Server)
public:
    explicit Server(QObject *parent = nullptr);
    void stopServer();
    void sendMessage(const QString &message);
protected:
    void incomingConnection(qintptr socketDescriptor) override;
signals:
    void messageReceived(const QString &message);
    void unlock();
private slots:
    void onReadyRead();
private:
    QTcpSocket *m_serverSocket;
};

#endif // SERVER_H
