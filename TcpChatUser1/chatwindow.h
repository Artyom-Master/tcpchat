#ifndef CHATWINDOW_H
#define CHATWINDOW_H

#include <QWidget>
#include <QAbstractSocket>
class ChatClient;
class Server;
class QStandardItemModel;
namespace Ui { class ChatWindow; }
class ChatWindow : public QWidget
{
    Q_OBJECT
    Q_DISABLE_COPY(ChatWindow)
public:
    explicit ChatWindow(QWidget *parent = nullptr);
    ~ChatWindow();
private:
    Ui::ChatWindow *ui;
    ChatClient *m_chatClient;
    Server *m_server;
    QStandardItemModel *m_chatModel;
    bool toConnect = true;
private slots:
    void toggleStartServer();
    void attemptConnection();
    void connectedToServer();
    void messageReceived(const QString &text);
    void sendMessage();
    void disconnectedFromServer();
    void unlockServerInterface();
    void error(QAbstractSocket::SocketError socketError);
};

#endif // CHATWINDOW_H
