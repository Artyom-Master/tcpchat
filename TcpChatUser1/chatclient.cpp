#include "chatclient.h"
#include <QTcpSocket>
#include <QDataStream>

ChatClient::ChatClient(QObject *parent)
    : QObject(parent)
    , m_clientSocket(new QTcpSocket(this))
{
    connect(m_clientSocket, &QTcpSocket::connected, this, &ChatClient::connected);
    connect(m_clientSocket, &QTcpSocket::disconnected, this, &ChatClient::disconnected);
    connect(m_clientSocket, &QTcpSocket::readyRead, this, &ChatClient::onReadyRead);
    connect(m_clientSocket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error), this, &ChatClient::error);
}

void ChatClient::sendMessage(const QString &text)
{
    if (text.isEmpty())
        return;
    QTextStream clientStream(m_clientSocket);
    clientStream << text;
}

void ChatClient::disconnectFromHost()
{
    m_clientSocket->disconnectFromHost();
}

void ChatClient::connectToServer(const QHostAddress &address, quint16 port)
{
    m_clientSocket->connectToHost(address, port);
}

void ChatClient::onReadyRead()
{
    QString data;
    QTextStream socketStream(m_clientSocket);
    data = socketStream.readAll();
    emit messageReceived(data);
}
